﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        Bitmap bmp;
        List<Point> Coordinates;
        // Флаг существования рисунка
        bool flag;
        Graphics g;
        Color borderColor;
        public Form1()
        {
            InitializeComponent();
            bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            g = Graphics.FromImage(bmp);
            flag = false;
            Coordinates = new List<Point>();
            borderColor = Color.FromArgb(255,0,0,255);
        }

        public void Bresenham4Line(Color clr, int x1, int y1, int x2, int y2)
        {
            Pen redBrush = new Pen(new SolidBrush(clr), 2);
            // Длина отрезка по X
            int deltaX = Math.Abs(x2 - x1);
            // Длина отрезка по Y
            int deltaY = Math.Abs(y2 - y1);
            int signX = x1 < x2 ? 1 : -1;
            int signY = y1 < y2 ? 1 : -1;
            int error = deltaX - deltaY;

            g.DrawRectangle(redBrush, x2, y2, 1, 1);

            while (x1 != x2 || y1 != y2)
            {
                g.DrawRectangle(redBrush, x1, y1, 1, 1);
                int error2 = error * 2;

                if (error2 > -deltaY)
                {
                    error -= deltaY;
                    x1 += signX;
                }
                if (error2 < deltaX)
                {
                    error += deltaX;
                    y1 += signY;
                }
            }
        }

        void LineFill(List<Point> polygon, Color conturColor, Color fillColor)
        {
            List<int> xCoords = new List<int>();
            int yMax = polygon[0].Y;
            int yMin = polygon[0].Y;
            foreach (var p in polygon)
            {
                if (p.Y > yMax) yMax = p.Y;
                if (p.Y < yMin) yMin = p.Y;
            }
            for (int y = yMin; y < yMax; y++)
            {
                for (int j = 0; j < polygon.Count; j++)
                {
                    Point p1 = polygon[j];
                    Point p2;
                    if (j == polygon.Count - 1) 
                    { 
                        p2 = polygon[0]; 
                    }
                    else 
                    { 
                        p2 = polygon[j + 1];
                    }
                    // Y коод. между вершинами точки
                    if (((p1.Y > y && p2.Y <= y) || (p1.Y <= y && p2.Y > y)) && p1.Y != p2.Y)
                    {
                        // Вектор направления по линии
                        int dx = p2.X - p1.X;
                        int dy = p2.Y - p1.Y;

                        // X коорд пересечения
                        int x = p1.X + (y - p1.Y) * dx / dy;
                        
                        if (((p1.Y > p2.Y) && (new Point(x, y) != p1)) || ((p2.Y > p1.Y) && (new Point(x, y) != p2)))
                        {
                            xCoords.Add(x);
                        }
                    }
                }
                xCoords.Sort();
                for (int j = 0; j < xCoords.Count - 1; j += 2)
                {
                    int left = xCoords[j];
                    int right = xCoords[j + 1];
                    Bresenham4Line(fillColor, left, y, right, y);
                }
                xCoords.Clear();
            }
        }

        private void pictureBox1_Click(object sender, MouseEventArgs e)
        {
            Pen black = new Pen(Color.Black);
            if (!flag)
            {
                var p = new Point(e.X, e.Y);
                if (e.Button == MouseButtons.Left)
                {
                    
                    Coordinates.Add(p);
                    if (Coordinates.Count == 1)
                    {
                        g.DrawRectangle(black, e.X, e.Y, 1, 1);
                    }
                    else 
                    {
                        Bresenham4Line(Color.Black, Coordinates[Coordinates.Count - 1].X, Coordinates[Coordinates.Count - 1].Y, Coordinates[Coordinates.Count - 2].X, Coordinates[Coordinates.Count - 2].Y);
                    }
                }
                else if (e.Button == MouseButtons.Right && Coordinates.Count > 2)
                {
                    LineFill(Coordinates, Color.Blue, Color.Blue);
                    flag = true;
                }
                pictureBox1.Image = bmp; 
            }
        }
        private void pictureBox1_ButtonClick(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Space)
            {
                g = Graphics.FromImage(pictureBox1.Image);
                Coordinates.Clear();
                g.Clear(Color.White);
                pictureBox1.Image = bmp;
                flag = false;

            }
        }
    }
}
